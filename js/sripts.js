

$(document).ready(function () {
    let index = 0;
    $('.sortable').sortable({ handle: 'button', cancel: '' }).disableSelection();;
    $('input[name="birthDay"]').datepicker({
        minDate: new Date(1900, 1 - 1, 1), maxDate: '-18Y',
        dateFormat: 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '-110:-18'
    });
    Object.keys(sessionStorage).forEach(function (key) {
        index++;
        var sessionEmployeeParams = new URLSearchParams(sessionStorage.getItem(key));
        var sessionEmployeeName = sessionEmployeeParams.get('name');
        $('.nav .sortable').append('<li><button data-index="' + index + '">' + sessionEmployeeName + '</button></li>');
        $('.wrapper').append(
            `
            <div id="`+ index + `" class="tabcontent">
                <p><span class="key">Employee Number</span><span class="value">`+ sessionEmployeeParams.get('number') + `</span></p>
                <p><span class="key">Employee Name</span><span class="value">`+ sessionEmployeeParams.get('name') + `</span></p>
                <p><span class="key">Employee Id</span><span class="value">`+ sessionEmployeeParams.get('id') + `</span></p>
                <p><span class="key">Employee birth day</span><span class="value">`+ sessionEmployeeParams.get('birthDay') + `</span></p>
                <p><span class="key">Employee gender</span><span class="value">`+ sessionEmployeeParams.get('gender') + `</span></p>
            </div>
        `);
    });

    function validateInput(pattern, input) {
        var inputValue = $(input).val();
        pattern = new RegExp(pattern);
        if ((pattern).test(inputValue)) {
            $(input).siblings('.error').css('display', 'none');
            return true;
        } else {
            $(input).siblings('.error').css('display', 'block');
            return false;
        }
    }
    $('form').on('submit', function (e) {
        e.preventDefault();
        //validate Employee Number
        validateInput('^[0-9-]{4,8}$', $('input[name="number"]'));

        //validate Employee Name
        validateInput('^([A-Za-z- ]{1,15})$', $('input[name="name"]'));

        //validate Employee Id
        validateInput('^([0-9]{1,15})$', $('input[name="id"]'));

        // Validate Date 
        if ($('input[name="birthDay"]').val().length > 1) {
              $('.dateError').css('display', 'none');
        }else {
            $('.dateError').css('display', 'block');
        }
         
        // Validate gender
        if ($("input[name='gender']").is(':checked')) {
            $('.checkError').css('display', 'none');
        } else {
            $('.checkError').css('display', 'block');
        }
        if (
            validateInput('^[0-9-]{4,8}$', $('input[name="number"]')) &&
            validateInput('^([A-Za-z- ]{1,15})$', $('input[name="name"]')) &&
            validateInput('^([0-9]{1,15})$', $('input[name="id"]')) &&
            $("input[name='gender']").is(':checked') &&
            $('input[name="birthDay"]').val().length > 1

        ) {
            // set new emplyee
            var newEmployeeObject = $('form').serialize();
            var employeeParams = new URLSearchParams(newEmployeeObject);
            var empNumber = employeeParams.get('number');
            sessionStorage.setItem(empNumber, newEmployeeObject);
            // display new employee
            index++;
            var newEmployeeName = employeeParams.get('name');
            $('.nav .sortable').append('<li><button data-index="' + index + '">' + newEmployeeName + '</button></li>');
            $('.wrapper').append(
                `
                <div id="`+ index + `" class="tabcontent">
                    <p><span class="key">Employee Number</span><span class="value">`+ employeeParams.get('number') + `</span></p>
                    <p><span class="key">Employee Name</span><span class="value">`+ employeeParams.get('name') + `</span></p>
                    <p><span class="key">Employee Id</span><span class="value">`+ employeeParams.get('id') + `</span></p>
                    <p><span class="key">Employee birth day</span><span class="value">`+ employeeParams.get('birthDay') + `</span></p>
                    <p><span class="key">Employee gender</span><span class="value">`+ employeeParams.get('gender') + `</span></p>
                </div>
            `);
            // clear form
            $(':input', 'form')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .prop('checked', false)
                .prop('selected', false);
            $('.form-error').css('display', 'none');
        } else {
            $('.form-error').css('display', 'block');
        }
    });
    $('.nav').on('click', 'button', function () {
        $('.tabcontent').css('display', 'none');
        $('#' + $(this).data('index')).css('display', 'block');
    })
});